package net.dongliu.vcdiff;

import net.dongliu.vcdiff.exception.VcdiffDecodeException;
import net.dongliu.vcdiff.exception.VcdiffEncodeException;
import org.junit.Ignore;
import org.junit.Test;

import java.io.*;

/**
 * @author dongliu
 */
public class DecodeTest {

    @Test
    @Ignore
    public void test() throws IOException, VcdiffDecodeException, VcdiffEncodeException {
//        VcdiffEncoder.encode(new File("/Users/dongliu/Documents/apks/qq.apk"),
//                new File("/Users/dongliu/Documents/apks/qq2.apk"),
//                new File("/Users/dongliu/Documents/apks/qq.patch"));
//        ByteArrayStream bas = new ByteArrayStream();
//        VcdiffDecoder.decode(new FileStream(new RandomAccessFile("/Users/dongliu/Documents/apks/qq.apk", "rw")),
//                new FileInputStream("/Users/dongliu/Documents/apks/qq.patch"),
//                bas);
//        FileOutputStream fos = new FileOutputStream("/Users/dongliu/Documents/apks/qq2b.apk");
//        fos.write(bas.toBytes());
//        fos.close();
    }

    @Test
    @Ignore
    public void test1() throws IOException, VcdiffDecodeException, VcdiffEncodeException {
        VcdiffDecoder.builder().source(new File("/Users/dongliu/code/java/jvcdiff/test_case/1/src.tar"))
                .patch(new File("/Users/dongliu/code/java/jvcdiff/test_case/1/delta.xdiff"))
                .decodeToFile(new File("/Users/dongliu/code/java/jvcdiff/test_case/1/merged.tar"));
    }
}
