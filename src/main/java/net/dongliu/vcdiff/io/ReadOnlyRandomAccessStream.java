package net.dongliu.vcdiff.io;

import net.dongliu.vcdiff.utils.IOUtils;

import java.io.Closeable;
import java.io.IOException;

/**
 * Read only random access stream.
 */
public abstract class ReadOnlyRandomAccessStream implements Closeable {

    /**
     * Sets the position for the next read.
     */
    public abstract void seek(int pos) throws IOException;

    /**
     * Add pos with offset
     */
    public void forward(int offset) throws IOException {
        seek(pos() + offset);
    }

    /**
     * get current pos.
     *
     * @throws IOException
     */
    public abstract int pos() throws IOException;

    public abstract int read(byte[] data, int offset, int length) throws IOException;

    /**
     * The data size of this stream
     *
     * @throws IOException
     */
    public abstract int length() throws IOException;

    /**
     * Get a readonly stream, from current pos, with length
     * the new data range: [pos, pos+offset).
     * side effect: pos += offset.
     *
     * @return a sliced stream, share the same data with origin buffer, if origin buffer is not reallocated.
     * @throws IOException
     */
    public abstract ReadOnlyRandomAccessStream slice(int length) throws IOException;

    /**
     * Read one next byte
     *
     * @return -1 if reach the end of stream, otherwise the next byte value in int
     * @throws IOException
     */
    public abstract int read() throws IOException;

    /**
     * Get byte value at index
     */
    public abstract byte get(int idx) throws IOException;
}
