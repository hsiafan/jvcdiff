package net.dongliu.vcdiff.io;

import java.io.Closeable;
import java.io.IOException;

/**
 * A stream has seek , length and pos method.
 *
 * @author dongliu
 */
public abstract class RandomAccessStream extends ReadOnlyRandomAccessStream {


    public abstract void write(byte[] data, int offset, int length) throws IOException;

    public abstract void write(byte[] data) throws IOException;

    public abstract void write(byte b) throws IOException;

    @Override
    public RandomAccessStream slice(int length) throws IOException {
        return null;
    }

    /**
     * If is read only
     *
     * @return
     */
    public abstract boolean readOnly();

//    /**
//     * Write data from ReadOnlyRandomAccessStream
//     * @param sourceStream
//     * @param size
//     */
//    public abstract void write(ReadOnlyRandomAccessStream sourceStream, int size);
}
