package net.dongliu.vcdiff.io;

/**
 * Random access stream can grow while write
 */
public abstract class ExpandableRandomAccessStream extends RandomAccessStream {

    @Override
    public boolean readOnly() {
        return false;
    }
}
