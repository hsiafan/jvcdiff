package net.dongliu.vcdiff.utils;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Utils for direct byte buffer
 */
public class ByteBufferUtils {

    /**
     * Try deallocate direct byte buffer, do nothing if buffer is not direct
     */
    public static boolean free(ByteBuffer byteBuffer) {
        if (byteBuffer == null || !byteBuffer.isDirect()) {
            return false;
        }

        //call ((DirectBuffer)byteBuffer).cleaner().clean() by reflection
        try {
            Method getCleanerMethod = byteBuffer.getClass().getDeclaredMethod("cleaner");
            getCleanerMethod.setAccessible(true);
            Object cleaner = getCleanerMethod.invoke(byteBuffer);
            if (cleaner != null) {
                Method cleanMethod = cleaner.getClass().getDeclaredMethod("clean");
                cleanMethod.setAccessible(true);
                cleanMethod.invoke(cleaner);
                return true;
            }
        } catch (Exception e) {
            return false;
        }

        return false;
    }

    /**
     * Create byte buffer from file. buffer is ready for read
     * @param file
     * @return
     * @throws IOException
     */
    public static ByteBuffer fileToByteBuffer(File file) throws IOException {
        long size = file.length();
        if (size > Integer.MAX_VALUE) {
            throw new UnsupportedOperationException("File is too larger");
        }
        ByteBuffer buffer = ByteBuffer.allocateDirect((int) size);
        try (RandomAccessFile raf = new RandomAccessFile(file, "r")) {
            // copy to memory at once to boost
            MappedByteBuffer mappedBuffer = raf.getChannel().map(FileChannel.MapMode.READ_ONLY, 0, size);
            buffer.put(mappedBuffer);
        }
        buffer.rewind();
        return buffer;
    }
}
