package net.dongliu.vcdiff;

import net.dongliu.vcdiff.exception.VcdiffDecodeException;
import net.dongliu.vcdiff.io.ByteArrayStream;
import net.dongliu.vcdiff.io.FixedByteArrayStream;
import net.dongliu.vcdiff.io.ReadOnlyRandomAccessStream;
import net.dongliu.vcdiff.utils.ByteBufferUtils;
import net.dongliu.vcdiff.utils.IOUtils;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Builder for vcdiff
 */
public class VcdiffDecoderBuilder {

    private ReadOnlyRandomAccessStream sourceStream;
    private ReadOnlyRandomAccessStream patchStream;

    VcdiffDecoderBuilder() {
    }

    /**
     * Set source from custom RandomAccessStream
     */
    public VcdiffDecoderBuilder source(ReadOnlyRandomAccessStream sourceStream) {
        this.sourceStream = sourceStream;
        return this;
    }

    /**
     * Set source from byte buffer.
     * Note: direct byte buffer will be de-allocated after used.
     */
    public VcdiffDecoderBuilder source(ByteBuffer byteBuffer) {
        return source(new FixedByteArrayStream(byteBuffer));
    }

    /**
     * Set source from byte array
     */
    public VcdiffDecoderBuilder source(byte[] bytes) {
        return source(ByteBuffer.wrap(bytes));
    }

    /**
     * Set source from File
     */
    public VcdiffDecoderBuilder source(File file) throws IOException {
        ByteBuffer buffer = ByteBufferUtils.fileToByteBuffer(file);
        return source(buffer);
    }


    /**
     * Set source from InputStream.
     * Note: input stream will be closed.
     */
    public VcdiffDecoderBuilder source(InputStream in) throws IOException {
        byte[] bytes = IOUtils.readAll(in);
        return source(bytes);
    }

    /**
     * Set patch from custom RandomAccessStream
     */
    public VcdiffDecoderBuilder patch(ReadOnlyRandomAccessStream patchStream) {
        this.patchStream = patchStream;
        return this;
    }

    /**
     * Set patch from byte buffer.
     * Note: direct byte buffer will be de-allocated after used.
     */
    public VcdiffDecoderBuilder patch(ByteBuffer byteBuffer) {
        return patch(new FixedByteArrayStream(byteBuffer));
    }

    /**
     * Set patch from byte array
     */
    public VcdiffDecoderBuilder patch(byte[] bytes) {
        return patch(ByteBuffer.wrap(bytes));
    }

    /**
     * Set patch from File
     */
    public VcdiffDecoderBuilder patch(File file) throws IOException {
        ByteBuffer buffer = ByteBufferUtils.fileToByteBuffer(file);
        return patch(buffer);
    }


    /**
     * Set patch from InputStream.
     * Note: input stream will be closed.
     */
    public VcdiffDecoderBuilder patch(InputStream in) throws IOException {
        byte[] bytes = IOUtils.readAll(in);
        return patch(bytes);
    }

    /**
     * Decode and write into byte array
     */
    public byte[] decodeToBytes() throws IOException, VcdiffDecodeException {
        int reasonableLength = sourceStream.length() + patchStream.length();
        try (ByteArrayStream bas = new ByteArrayStream(reasonableLength)) {
            new VcdiffDecoder(sourceStream, patchStream, bas).decode();
            return bas.toBytes();
        }
    }

    /**
     * Decode and write into byte array
     */
    public void decodeToStream(OutputStream out) throws IOException, VcdiffDecodeException {
        int reasonableLength = sourceStream.length() + patchStream.length();
        try (ByteArrayStream bas = new ByteArrayStream(reasonableLength)) {
            out.write(bas.toBytes());
        }
    }

    /**
     * Decode and write into byte array
     */
    public void decodeToFile(File targetFile) throws IOException, VcdiffDecodeException {
        int reasonableLength = sourceStream.length() + patchStream.length();
        try (ByteArrayStream bas = new ByteArrayStream(reasonableLength)) {
            new VcdiffDecoder(sourceStream, patchStream, bas).decode();
            ByteBuffer buffer = bas.getBuffer();
            try (FileOutputStream fos = new FileOutputStream(targetFile);
                 FileChannel channel = fos.getChannel()) {
                channel.write(buffer);
            }
        }
    }
}
